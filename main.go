package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

type variable struct {
	VariableType string `json:"variable_type"`
	Key          string `json:"key"`
	Value        string `json:"value"`
}

const defaultProject = "16421069"

func main() {
	if err := run(); err != nil {
		log.Println(err)
	}
}

func run() error {
	token := os.Args[1]
	project := defaultProject
	if len(os.Args) == 3 {
		project = os.Args[2]
	}
	handleErr := func(err error) error {
		return fmt.Errorf("run: %w", err)
	}
	if err := os.Mkdir("env", 0700); err != nil {
		return handleErr(err)
	}
	dotenv, err := os.OpenFile("./env/dot.env", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0400)
	if err != nil {
		return handleErr(err)
	}
	defer dotenv.Close()
	for page := 1; ; page++ {
		req, err := http.NewRequest(
			http.MethodGet, fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/variables?per_page=100&page=%d", project, page),
			nil,
		)
		if err != nil {
			return handleErr(err)
		}
		req.Header.Add("PRIVATE-TOKEN", token)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return handleErr(err)
		}
		var vars []variable
		if err := json.NewDecoder(resp.Body).Decode(&vars); err != nil {
			return handleErr(err)
		}
		if len(vars) == 0 {
			return nil
		}
		processVars(dotenv, vars)
	}
}

func processVars(dotenv io.StringWriter, vars []variable) error {
	handleErr := func(err error) error {
		return fmt.Errorf("process vars: %w", err)
	}
	for _, v := range vars {
		if v.VariableType != "file" {
			if _, err := dotenv.WriteString(v.Key + "=" + v.Value + "\n"); err != nil {
				return handleErr(err)
			}
			continue
		}
		if err := os.WriteFile("./env/"+v.Key, []byte(v.Value), 0400); err != nil {
			return handleErr(err)
		}
		if _, err := dotenv.WriteString(v.Key + "=./" + v.Key + "\n"); err != nil {
			return handleErr(err)
		}
	}
	return nil
}
